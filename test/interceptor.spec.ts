import { Test, TestingModule } from '@nestjs/testing';
import { AuthInterceptor } from '../src/auth/auth.interceptor';
import { JwtService } from '@nestjs/jwt';

describe('AuthInterceptor', () => {
  let interceptor: AuthInterceptor;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthInterceptor,
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
            verify: jest.fn(),
          },
        },
      ],
    }).compile();

    interceptor = module.get<AuthInterceptor>(AuthInterceptor);
  });

  it('should be defined', () => {
    expect(interceptor).toBeDefined();
  });

  it('should add access token from refresh token', () => {
    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            'refresh-token': 'valid_refresh_token_here',
            authorization: 'Bearer ',
          },
        }),
      }),
    };
    const next = {
      handle: () => ({
        subscribe: () => null,
      }),
    };
    const setHeaderSpy = jest.spyOn(next, 'handle').mockReturnValue({
      subscribe: () => null,
    });
    interceptor.intercept(context as any, next as any);
    expect(setHeaderSpy).toHaveBeenCalledWith();
    expect(context.switchToHttp().getRequest().headers.authorization).toContain(
      'Bearer ',
    );
  });

  it('should not add access token if refresh token is missing', () => {
    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {},
        }),
      }),
    };
    const next = {
      handle: () => ({
        subscribe: () => null,
      }),
    };
    const setHeaderSpy = jest.spyOn(next, 'handle').mockReturnValue({
      subscribe: () => null,
    });
    interceptor.intercept(context as any, next as any);
    expect(setHeaderSpy).toHaveBeenCalledWith();
    expect(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      context.switchToHttp().getRequest().headers.authorization,
    ).toBeUndefined();
  });
});
