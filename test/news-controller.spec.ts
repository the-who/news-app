import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from '../src/news/news.controller';
import { NewsService } from '../src/news/news.service';
import { User } from '../src/user/user.entity';
import { News } from '../src/news/news.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

describe('NewsController', () => {
  let controller: NewsController;
  let newsService: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [
        NewsService,
        {
          provide: getRepositoryToken(News),
          useClass: Repository,
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
            verify: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<NewsController>(NewsController);
    newsService = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should find all news', async () => {
    const newsList = [
      { id: 1, title: 'News 1', content: 'This is news 1.', author: { id: 1 } },
      { id: 2, title: 'News 2', content: 'This is news 2.', author: { id: 2 } },
    ];
    newsService.findAll = jest.fn().mockResolvedValue(newsList);

    const result = await controller.findAll();
    expect(newsService.findAll).toHaveBeenCalled();
    expect(result).toEqual(newsList);
  });

  it('should find one news by ID', async () => {
    const newsId = 1;
    const news = {
      id: newsId,
      title: 'Test News',
      content: 'This is a test news article.',
      author: { id: 1 },
    };
    newsService.findOne = jest.fn().mockResolvedValue(news);

    const result = await controller.findOne(newsId);
    expect(newsService.findOne).toHaveBeenCalledWith(newsId);
    expect(result).toEqual(news);
  });

  it('should create a new news', async () => {
    const newUser: User = {
      id: 1,
      email: 'test@example.com',
      password: 'password',
      news: [],
    };
    const newNews = {
      title: 'Test News',
      content: 'This is a test news article.',
      author: newUser,
    };
    const createdNews = { id: 1, ...newNews };
    newsService.create = jest.fn().mockResolvedValue(createdNews);

    const result = await controller.create(newNews);
    expect(newsService.create).toHaveBeenCalledWith(newNews);
    expect(result).toEqual(createdNews);
  });

  it('should update an existing news', async () => {
    const newsId = 1;
    const updateData = {
      title: 'Updated News Title',
      content: 'This is an updated news article.',
    };
    const updatedNews = { id: newsId, ...updateData };
    newsService.update = jest.fn().mockResolvedValue(updatedNews);

    const result = await controller.update(newsId, updateData);
    expect(newsService.update).toHaveBeenCalledWith(newsId, updateData);
    expect(result).toEqual(updatedNews);
  });

  it('should delete an existing news', async () => {
    const newsId = 1;
    newsService.delete = jest.fn();

    await controller.delete(newsId);
    expect(newsService.delete).toHaveBeenCalledWith(newsId);
  });
});
