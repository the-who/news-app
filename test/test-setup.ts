import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppModule } from '../src/app.module';
import { Connection } from 'typeorm';
import { databaseConfig } from '../src/db-config';

let app: INestApplication;
let connection: Connection;

beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [
      AppModule,
      TypeOrmModule.forRoot({
        ...databaseConfig,
        keepConnectionAlive: true,
      }),
    ],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();
  connection = app.get(Connection);
});

afterAll(async () => {
  await app.close();
});

beforeEach(async () => {
  // Clear database tables before each test
  const entities = connection.entityMetadatas;

  for (const entity of entities) {
    const repository = connection.getRepository(entity.name);
    const query = `DELETE FROM "${entity.tableName}"`;
    await repository.query(query);
  }
});

export { app };
