import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '../src/user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../src/user/user.entity';

describe('UserService', () => {
  let service: UserService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
    userRepository = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new user', async () => {
    const newUser = { email: 'test@example.com', password: 'password' };
    const savedUser = { id: 1, ...newUser };
    userRepository.create = jest.fn().mockReturnValue(savedUser);
    userRepository.save = jest.fn().mockReturnValue(savedUser);

    const result = await service.create(newUser);
    expect(userRepository.create).toHaveBeenCalledWith(newUser);
    expect(userRepository.save).toHaveBeenCalledWith(savedUser);
    expect(result).toEqual(savedUser);
  });

  it('should find a user by email', async () => {
    const user = { id: 1, email: 'test@example.com', password: 'password' };
    userRepository.findOne = jest.fn().mockResolvedValue(user);

    const result = await service.findByEmail('test@example.com');
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: {
        email: 'test@example.com',
      },
    });
    expect(result).toEqual(user);
  });

  it('should return undefined when user not found by email', async () => {
    userRepository.findOne = jest.fn().mockResolvedValue(undefined);

    const result = await service.findByEmail('test@example.com');
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: {
        email: 'test@example.com',
      },
    });
    expect(result).toBeUndefined();
  });
});
