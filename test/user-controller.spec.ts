import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from '../src/user/user.controller';
import { UserService } from '../src/user/user.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../src/user/user.entity';
import { Repository } from 'typeorm';

describe('UserController', () => {
  let controller: UserController;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
    userService = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a new user', async () => {
    const newUser = { email: 'test@example.com', password: 'password' };
    const savedUser = { id: 1, ...newUser };
    userService.create = jest.fn().mockResolvedValue(savedUser);

    const result = await controller.createUser(newUser);
    expect(userService.create).toHaveBeenCalledWith(newUser);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { password, ...savedExpected } = savedUser;
    expect(result).toEqual(savedExpected);
  });
});
