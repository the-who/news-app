import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from '../src/news/news.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { News } from '../src/news/news.entity';
import { User } from 'src/user/user.entity';

describe('NewsService', () => {
  let service: NewsService;
  let newsRepository: Repository<News>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: getRepositoryToken(News),
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
    newsRepository = module.get<Repository<News>>(getRepositoryToken(News));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create a new news', async () => {
    const newUser: User = {
      id: 1,
      email: 'test@example.com',
      password: 'password',
      news: [],
    };
    const newNews = {
      title: 'Test News',
      content: 'This is a test news article.',
      author: newUser,
    };

    const savedNews = { id: 1, ...newNews };
    newsRepository.create = jest.fn().mockReturnValue(savedNews);
    newsRepository.save = jest.fn().mockReturnValue(savedNews);

    const result = await service.create(newNews);
    expect(newsRepository.create).toHaveBeenCalledWith(newNews);
    expect(newsRepository.save).toHaveBeenCalledWith(savedNews);
    expect(result).toEqual(savedNews);
  });

  it('should update an existing news', async () => {
    const newsId = 1;
    const updateData = {
      title: 'Updated News Title',
      content: 'This is an updated news article.',
    };
    const existingNews = {
      id: newsId,
      title: 'Test News',
      content: 'This is a test news article.',
      author: { id: 1 },
    };
    const updatedNews = { id: newsId, ...existingNews, ...updateData };
    newsRepository.findOne = jest.fn().mockResolvedValue(existingNews);
    newsRepository.update = jest.fn();
    newsRepository.findOne = jest.fn().mockResolvedValue(updatedNews);

    const result = await service.update(newsId, updateData);
    expect(newsRepository.findOne).toHaveBeenCalledWith({ where: { id: 1 } });
    expect(newsRepository.update).toHaveBeenCalledWith(newsId, updateData);
    expect(newsRepository.findOne).toHaveBeenCalledTimes(1);
    expect(result).toEqual(updatedNews);
  });

  it('should delete an existing news', async () => {
    const newsId = 1;
    newsRepository.delete = jest.fn().mockResolvedValue({ affected: 1 });

    await service.delete(newsId);
    expect(newsRepository.delete).toHaveBeenCalledWith(newsId);
  });

  it('should find all news', async () => {
    const newsList = [
      { id: 1, title: 'News 1', content: 'This is news 1.', author: { id: 1 } },
      { id: 2, title: 'News 2', content: 'This is news 2.', author: { id: 2 } },
    ];
    newsRepository.find = jest.fn().mockResolvedValue(newsList);

    const result = await service.findAll();
    expect(newsRepository.find).toHaveBeenCalled();
    expect(result).toEqual(newsList);
  });

  it('should find one news by ID', async () => {
    const newsId = 1;
    const news = {
      id: newsId,
      title: 'Test News',
      content: 'This is a test news article.',
      author: { id: 1 },
    };
    newsRepository.findOne = jest.fn().mockResolvedValue(news);

    const result = await service.findOne(newsId);
    expect(newsRepository.findOne).toHaveBeenCalledWith({ where: { id: 1 } });
    expect(result).toEqual(news);
  });

  it('should return undefined when news not found by ID', async () => {
    const newsId = 1;
    newsRepository.findOne = jest.fn().mockResolvedValue(undefined);

    const result = await service.findOne(newsId);
    expect(newsRepository.findOne).toHaveBeenCalledWith({ where: { id: 1 } });
    expect(result).toBeUndefined();
  });
});
