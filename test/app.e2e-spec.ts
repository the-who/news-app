import * as request from 'supertest';
import { app } from './test-setup';
import { HttpStatus } from '@nestjs/common';

describe('Tests (e2e)', () => {
  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('should create a user', async () => {
    const userData = { email: 'test@example.com', password: 'testpassword' };

    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(userData)
      .expect(201);

    const { body } = response;
    expect(body).toHaveProperty('email', userData.email);
    expect(body).not.toHaveProperty('password');
  });

  it('should return 400 for missing email', async () => {
    const userData = { password: 'testpassword' };

    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(userData)
      .expect(400);

    const { body } = response;
    expect(body).toHaveProperty('message', ['email must be an email']);
  });

  it('should return 400 for missing password', async () => {
    const userData = { email: 'test@example.com' };

    const response = await request(app.getHttpServer())
      .post('/auth/register')
      .send(userData)
      .expect(400);

    const { body } = response;
    expect(body).toHaveProperty('message', [
      'password must be a string',
      'password should not be empty',
    ]);
  });
  it('should create a new news item', async () => {
    const userData = { email: 'test@example.com', password: 'testpassword' };
    await request(app.getHttpServer())
      .post('/auth/register')
      .send(userData)
      .expect(HttpStatus.CREATED);
    const loginResponse = await request(app.getHttpServer())
      .post('/auth/login')
      .send(userData);

    const accessToken = loginResponse.body.accessToken;

    const newsData = {
      title: 'Test News',
      content: 'This is a test news item',
    };
    const response = await request(app.getHttpServer())
      .post('/news')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(newsData)
      .expect(HttpStatus.CREATED);

    expect(response.body).toHaveProperty('title', newsData.title);
    expect(response.body).toHaveProperty('content', newsData.content);
  });
  it('should return 401 for bad authentication', async () => {
    const newsData = {
      title: 'Test News',
      content: 'This is a test news item',
    };

    const accessToken = 'invalid_access_token';

    const response = await request(app.getHttpServer())
      .post('/news')
      .set('Authorization', `Bearer ${accessToken}`)
      .send(newsData)
      .expect(HttpStatus.FORBIDDEN);

    expect(response.body).toHaveProperty('message', 'Forbidden resource');
  });
  it('should refresh the access token', async () => {
    const userData = { email: 'test@example.com', password: 'testpassword' };
    await request(app.getHttpServer()).post('/auth/register').send(userData);

    const loginResponse = await request(app.getHttpServer())
      .post('/auth/login')
      .send(userData);

    const accessToken = loginResponse.body.accessToken;
    const refreshToken = loginResponse.body.refreshToken;

    await new Promise((resolve) => setTimeout(resolve, 2000));

    const refreshResponse = await request(app.getHttpServer())
      .post('/auth/refresh')
      .send({ refreshToken })
      .expect(HttpStatus.CREATED);

    const newAccessToken = refreshResponse.body.accessToken;

    expect(newAccessToken).not.toEqual(accessToken);
  });

  it('should return 401 for tampered refresh token', async () => {
    const userData = { email: 'test@example.com', password: 'testpassword' };
    await request(app.getHttpServer()).post('/auth/register').send(userData);
    const loginResponse = await request(app.getHttpServer())
      .post('/auth/login')
      .send(userData);
    const refreshToken = loginResponse.body.refreshToken;

    const tamperedRefreshToken = refreshToken + 'random_characters';

    const response = await request(app.getHttpServer())
      .post('/auth/refresh')
      .send({ refreshToken: tamperedRefreshToken })
      .expect(HttpStatus.UNAUTHORIZED);

    expect(response.body).toHaveProperty('message', 'Invalid refresh token');
  });
});
