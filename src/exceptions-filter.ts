import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger,
} from '@nestjs/common';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private readonly logger = new Logger(AllExceptionsFilter.name);

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();

    let status = HttpStatus.INTERNAL_SERVER_ERROR;
    let message = 'Internal server error';

    this.logger.log('2222222222222');
    if (exception instanceof HttpException) {
      this.logger.log(exception);
      this.logger.log('1111111111111');
      status = exception.getStatus();
      message = exception.message;
    }

    this.logger.error(`${message} ${status}`);

    response.status(status).json({
      statusCode: status,
      message,
    });
  }
}
