import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from '../user/user.entity';
import { UserService } from '../user/user.service';
import { ConfigService } from '../config/config.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async register(email: string, password: string): Promise<User> {
    const hashedPassword = await bcrypt.hash(password, 10);
    return this.userService.create({ email, password: hashedPassword });
  }

  async validateUser(email: string, password: string): Promise<User | null> {
    const user = await this.userService.findByEmail(email);
    if (user && (await bcrypt.compare(password, user.password))) {
      return user;
    }
    return null;
  }

  async generateAccessToken(user: User): Promise<string> {
    const payload = { sub: user.id };
    return this.generateToken(payload, '3m');
  }

  async generateRefreshToken(user: User): Promise<string> {
    const payload = { sub: user.id };
    return this.generateToken(payload, '3d');
  }

  async generateToken(payload: any, expiresIn: string) {
    const secret = this.configService.getSecret();
    return this.jwtService.signAsync(payload, {
      expiresIn,
      secret,
    });
  }

  async refreshTokens(
    refreshToken: string,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    try {
      const secret = this.configService.getSecret();
      const decoded = this.jwtService.verify(refreshToken, {
        secret,
      });
      const userId = decoded.sub;

      const user = await this.userService.findById(userId);

      if (!user) {
        throw new Error('User not found');
      }

      const accessToken = await this.generateAccessToken(user);

      const newRefreshToken = await this.generateRefreshToken(user);

      return { accessToken, refreshToken: newRefreshToken };
    } catch (error) {
      throw new HttpException('Invalid refresh token', HttpStatus.UNAUTHORIZED);
    }
  }
}
