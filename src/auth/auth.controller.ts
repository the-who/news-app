import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../user/user.entity';
import { UserDto } from './auth.dto';

@Controller('auth')
@UsePipes(new ValidationPipe())
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('register')
  async register(@Body() body: UserDto): Promise<User> {
    const result = await this.authService.register(body.email, body.password);
    result.password = undefined;
    return result;
  }

  @Post('login')
  async login(@Body() body: UserDto): Promise<any> {
    const user = await this.authService.validateUser(body.email, body.password);
    if (!user) throw new Error('Invalid credentials');
    const accessToken = await this.authService.generateAccessToken(user);
    const refreshToken = await this.authService.generateRefreshToken(user);
    return { accessToken, refreshToken };
  }

  @Post('refresh')
  async refresh(@Body() body: { refreshToken: string }): Promise<any> {
    const { accessToken, refreshToken } = await this.authService.refreshTokens(
      body.refreshToken,
    );

    return { accessToken, refreshToken };
  }
}
