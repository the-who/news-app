import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';

config();

export const isValidEnv = (env: { [key: string]: unknown }) => {
  return Object.keys(env).reduce((isValid: boolean, key: string) => {
    if (env[key] === undefined) {
      console.error(`Please set correct config/env variable: ${key}`);
      return false;
    }
    return isValid;
  }, true);
};

function getRequiredEnv() {
  return {
    POSTGRES_USER: process.env.POSTGRES_USER,
    POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
    POSTGRES_DB: process.env.POSTGRES_DB,
    POSTGRES_HOST: process.env.POSTGRES_HOST,
    SALT: process.env.SALT,
    SECRET: process.env.SECRET,
  };
}

function prepareEnv() {
  const requiredEnv = getRequiredEnv();

  if (!isValidEnv(requiredEnv)) {
    return process.exit(1);
  }

  return {
    ...requiredEnv,
    POSTGRES_PORT: process.env.POSTGRES_PORT,
    POSTGRES_ENABLE_SSL: process.env.POSTGRES_ENABLE_SSL,
    SWAGGER_BASE_PATH: process.env.SWAGGER_BASE_PATH,
    PORT: process.env.PORT,
  };
}

@Injectable()
export class ConfigService {
  private readonly envConfig = prepareEnv();

  getPostgresConfig() {
    return {
      POSTGRES_USER: this.envConfig.POSTGRES_USER,
      POSTGRES_PASSWORD: this.envConfig.POSTGRES_PASSWORD,
      POSTGRES_DB: this.envConfig.POSTGRES_DB,
      POSTGRES_PORT: this.envConfig.POSTGRES_PORT,
      POSTGRES_HOST: this.envConfig.POSTGRES_HOST,
      POSTGRES_ENABLE_SSL: this.envConfig.POSTGRES_ENABLE_SSL,
    };
  }

  getSalt() {
    return this.envConfig.SALT;
  }

  getSecret() {
    return this.envConfig.SECRET;
  }
}
