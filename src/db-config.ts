import { join } from 'path';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const REQUEST_TIMEOUT = 120000;

export const databaseConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  ssl: Boolean(process.env.POSTGRES_ENABLE_SSL),
  entities: [join(__dirname, '../**/*.entity.ts')],
  migrations: [join(__dirname, './migrations', '/*{.ts,.js}')],
  migrationsRun: true,
  connectTimeoutMS: REQUEST_TIMEOUT,
  maxQueryExecutionTime: REQUEST_TIMEOUT,
  extra: {
    query_timeout: REQUEST_TIMEOUT,
  },
};
