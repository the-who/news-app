import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  UseGuards,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { News } from './news.entity';
import { JwtAuthGuard } from '../auth/jwt.guard';

@Controller('news')
@UsePipes(new ValidationPipe())
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get()
  async findAll(): Promise<News[]> {
    return this.newsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<News> {
    return this.newsService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() body: Partial<News>): Promise<News> {
    return this.newsService.create(body);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: Partial<News>,
  ): Promise<News> {
    return this.newsService.update(id, body);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<void> {
    return this.newsService.delete(id);
  }
}
