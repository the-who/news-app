import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { News } from './news.entity';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private newsRepository: Repository<News>,
  ) {}

  async create(newsData: Partial<News>): Promise<News> {
    const newNews = this.newsRepository.create(newsData);
    return this.newsRepository.save(newNews);
  }

  async update(id: number, newsData: Partial<News>): Promise<News> {
    await this.newsRepository.update(id, newsData);
    return this.newsRepository.findOne({ where: { id } });
  }

  async delete(id: number): Promise<void> {
    await this.newsRepository.delete(id);
  }

  async findAll(): Promise<News[]> {
    return this.newsRepository.find();
  }

  async findOne(id: number): Promise<News> {
    return this.newsRepository.findOne({ where: { id } });
  }
}
